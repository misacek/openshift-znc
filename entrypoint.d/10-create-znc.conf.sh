#!/usr/bin/env bash

function log(){
    local MY_NAME=${MY_NAME:-$0}
    logger -t ${MY_NAME} -- $@
    echo "$(date) ${MY_NAME}: $@"
}


function trap_exit(){
    log "Trapped error on line $(caller)"
    log "result: failure"
    exit 1
}


function create_znc_conf(){
    trap trap_exit ERR

    # this file should be always created by Dockerfile during build
    [ -f $ZNC_CONFIG_BASE ]

    # copy znc config from default location to (possibly persistent) other
    # location. It cannot be there from the start because if persistent volume is
    # mounted there the content is overwritten.
    log create_znc_conf "----> id: $(id)"
    log create_znc_conf "----> ZNC_DATADIR: $(ls -ld ${ZNC_DATADIR})"
    mkdir -p $(dirname ${ZNC_CONFIG})
    cp ${ZNC_CONFIG_BASE} ${ZNC_CONFIG}
}

function get_hash(){
    trap trap_exit ERR

    [ -n "$1" ]
    # credits go to: https://stackoverflow.com/a/22901380

}

function create_certificates(){
    trap trap_exit ERR
    [ -n "$ZNC_SSL_CERTIFICATE_HOSTNAME" ]
    /usr/bin/openssl version > /dev/null

    if [ "${ZNC_USE_SSL}" != 'no' ];
    then
        log create_certificates: "----> SSL will be configured"
        # create certificates if they are not already present
        if [ ! -f ${ZNC_DATADIR}/certs/cert.pem ]  ||[ ! -f ${ZNC_DATADIR}/certs/private-key.pem ];
        then
            log "----> creating self-signed SSL certificates"
            mkdir -p ${ZNC_DATADIR}/certs
            /usr/bin/openssl req -new -x509 -nodes -days 3650 -newkey rsa:4096 \
                -keyout ${ZNC_DATADIR}/certs/private-key.pem \
                -out ${ZNC_DATADIR}/certs/cert.pem -subj \
                "/CN=$ZNC_SSL_CERTIFICATE_HOSTNAME"
        else
            log create_certificates: "----> using already present SSL certificates from ${ZNC_DATADIR}/certs/"
        fi

        # create key+cert if it is not already there
        cat \
            ${ZNC_DATADIR}/certs/cert.pem \
            ${ZNC_DATADIR}/certs/private-key.pem \
        > ${ZNC_DATADIR}/key+cert.pem

        echo "SSLCertFile = ${ZNC_DATADIR}/key+cert.pem" >> ${ZNC_CONFIG}
        sed 's/SSL = .*/SSL = true/g' -i ${ZNC_CONFIG}
    else
        log create_certificates: "----> Not using SSL."
        sed -ie '/SSL.*/d' ${ZNC_CONFIG}
    fi
}

function create_listeners(){
    trap trap_exit ERR

    [ -w ${ZNC_CONFIG} ] || touch ${ZNC_CONFIG}
    [ -d ${ZNC_CONFIG_D}/listeners ]

    for listener in ${ZNC_CONFIG_D}/listeners/*;
    do
        echo "<Listener $(basename $listener)>" >> $ZNC_CONFIG
        cat $listener >> $ZNC_CONFIG
        echo "</Listener>" >> $ZNC_CONFIG

    done
}

function create_users(){
    trap trap_exit ERR

    [ -w ${ZNC_CONFIG} ] || touch ${ZNC_CONFIG}
    [ -d ${ZNC_CONFIG_D}/users ]
    /usr/bin/openssl version > /dev/null

    for user in ${ZNC_CONFIG_D}/users/*;
    do
        echo "<User $(basename $user)>" >> $ZNC_CONFIG
        cat $user >> $ZNC_CONFIG

        # plaintext passwor for user "misacek" should be stored in env variable
        # $ZNC_PASSWORD_FOR_USER_misacek or will get default value 'password'
        VARNAME="ZNC_PASSWORD_FOR_USER_$(basename $user)"
        PASSWORD=${!VARNAME}
        [ -z "$PASSWORD" ] && PASSWORD=password
        (echo $PASSWORD; echo $PASSWORD;) | znc --makepass | grep \<Pass -A 4 >> $ZNC_CONFIG

        echo "</User>" >> $ZNC_CONFIG
    done
}


trap trap_exit ERR
set -e
[ -n "$DEBUG" ] && set -x


#export MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir

# directory where base for znc is (should contain at least configs/
export ZNC_DATADIR=${ZNC_DATADIR:-'/data'}

# whether or not use ssl for the listener
export ZNC_USE_SSL=${ZNC_USE_SSL:-'yes'}

# name for the generated self-signed certificate
export ZNC_SSL_CERTIFICATE_HOSTNAME=${ZNC_SSL_CERTIFICATE_HOSTNAME:-'localhost'}

# --

export SALT=$(/usr/bin/openssl rand -base64 10 2>/dev/null)

# this file is created when building image from Dockefile
export ZNC_CONFIG_BASE=${ZNC_CONFIG_BASE:-"/znc.conf-BASE"}

# resulting image that will be used by znc
export ZNC_CONFIG=${ZNC_CONFIG:-"${ZNC_DATADIR}/configs/znc.conf"}

# contains configurationo for users/ and listeners/
export ZNC_CONFIG_D=${ZNC_CONFIG_D:-'/znc.conf.d'}


if [ "${#@}" -gt 0 ];
then
    $@
else
        create_znc_conf
        create_certificates
        create_listeners
        create_users
fi

if [ -n "$DEBUG" ];
then
    env | sort
    nl $ZNC_CONFIG
    #ls -lR $ZNC_DATADIR
fi

log "-----> success of $0"
