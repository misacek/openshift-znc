#!/bin/bash

if [ -z "$@" ];
then
    for a in /entrypoint.d/*; do $a || exit 1; done
else
    exec "$@"
fi
