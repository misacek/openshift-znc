
include Makefile.portability


SERVICE_NAME ?= znc

HAVE_GIT := $(shell git --version 2>/dev/null)
ifdef HAVE_GIT
LAST_COMMIT := $(shell git rev-parse --short HEAD)
LAST_COMMIT_UNIX_TIMESTAMP := $(shell git show -s --format=%ct $(LAST_COMMIT))
LAST_COMMIT_DATE := $(shell $(DATE) --date=@$(LAST_COMMIT_UNIX_TIMESTAMP) +%Y%m%d%H%M%S)
else
LAST_COMMIT := unknown-commit
endif

LABEL_BUILD_DATE := $(shell $(DATE) --rfc-3339=seconds)
LABEL_VERSION := $(LAST_COMMIT_DATE)-$(LAST_COMMIT)

BUILD_ENV :=
BUILD_ENV += --build-arg LABEL_BUILD_DATE="$(LABEL_BUILD_DATE)"
BUILD_ENV += --build-arg LABEL_VCS_REF="$(LAST_COMMIT)"
BUILD_ENV :=

build:
	docker-compose build $(BUILD_ENV) $(SERVICE_NAME)

start: run
run:
	docker-compose up -d $(SERVICE_NAME)
	sleep 2
	docker-compose logs $(SERVICE_NAME)
	@echo "You should be able to access $(SERVICE_NAME) on https://127.0.0.1:12122"

stop:
	docker-compose stop

push:
	docker-compose push $(SERVICE_NAME)

pull:
	docker-compose pull $(SERVICE_NAME)

PAYLOAD ?= /tmp/payload.json
payload: $(PAYLOAD)
$(PAYLOAD):
	@echo "git:" > $(PAYLOAD)
	@echo "  uri: $(CI_REPOSITORY_URL)" >> $(PAYLOAD)
	@echo "  ref: $(CI_BUILD_REF_NAME)" >> $(PAYLOAD)
#       @echo "  commit: placeholder-CI_BUILD_REF " >> $(PAYLOAD)
	@echo "  author:" >> $(PAYLOAD)
	@echo "    name: $(shell git show --format=%an --quiet $(LABEL_VCS_REF))" >> $(PAYLOAD)
	@echo "    email: $(shell git show --format=%ae --quiet $(LABEL_VCS_REF))" >> $(PAYLOAD)
	@echo "  commiter:" >> $(PAYLOAD)
	@echo "    name: $(shell git show --format=%cn --quiet $(LABEL_VCS_REF))" >> $(PAYLOAD)
	@echo "    email: $(shell git show --format=%ce --quiet $(LABEL_VCS_REF))" >> $(PAYLOAD)
	@echo "  message: \"$(shell git show --format=%s --quiet $(LABEL_VCS_REF))\"" >> $(PAYLOAD)
	@echo "env:" >> $(PAYLOAD)
	@echo "  - name: LABEL_VERSION" >> $(PAYLOAD)
	@echo "    value: \"$(LABEL_VERSION)\"" >> $(PAYLOAD)
	@echo "  - name: GIT_SSL_NO_VERIFY" >> $(PAYLOAD)
	@echo "    value: \"true\"" >> $(PAYLOAD)
	@echo $(shell date) Makefile: success: $@
.PHONY: payload # creates $(PAYLOAD) file so it can be submitted to openshift generict trigger
.PHONY: $(PAYLOAD)

.PHONY: test
