#!/bin/bash

function log(){
    local MY_NAME=${MY_NAME:-$0}
    logger -t ${MY_NAME} -- $@
    echo "$(date) ${MY_NAME}: $@"
}


function trap_exit(){
    log "Trapped error on line $(caller)"
    log "result: failure"
    exit 1
}


function route(){
    trap trap_exit ERR

    [ -f ssl-dummy-keys/key.pem ]
    [ -f ssl-dummy-keys/cert.pem ]

    ROUTE=(
    oc create route edge \
      --key=ssl-dummy-keys/key.pem
      --cert=ssl-dummy-keys/cert.pem
      --ca-cert=ssl-dummy-keys/cert.pem
      --service=znc
      --hostname=znc.127.0.0.1.nip.io
    )
    ${ROUTE[@]}

    log 'route: success'
}

function new_app(){
    trap trap_exit ERR

    APP=(
        oc new-app $GITURL#$GITBRANCH --name=znc
          --build-env=GIT_SSL_NO_VERIFY=true
          --build-env=DEBUG=1
    )
    ${APP[@]}

#oc env bc/znc GIT_SSL_NO_VERIFY=true
#oc env bc/znc DEBUG=1

    log 'new_app: success'
}

function probes(){
    trap trap_exit ERR

    oc set probe dc/znc --readiness --get-url=http://:12121
    oc set probe dc/znc --liveness -- echo ok

    log 'success: probes'

}

function project(){
    trap trap_exit ERR

    oc delete project znc
    oc new-project znc

    log 'success: project'

}

trap trap_exit ERR
[ -n "$DEBUG" ] && set -x

export MY_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") #full path to this dir
export GITURL=https://gitlab.cee.redhat.com/mnovacek/znc-openshift.git
export GITBRANCH=znc-image-based

if [ "$#" -eq 0 ];
then
    new_app
    probes
    route
else
    $@
fi

echo Success.
