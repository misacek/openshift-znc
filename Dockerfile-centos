
FROM centos:7

ARG LABEL_BUILD_DATE
ARG LABEL_URL=https://znc.cloud.upshift.engineering.redhat.com
ARG LABEL_VCS_REF
ARG LABEL_VCS_TYPE=git
ARG LABEL_VCS_URL=https://gitlab.cee.redhat.com/mnovacek/openshift-znc
ARG DEBUG=1

ARG CONTAINER_USER_UID=5001
ARG CONTAINER_USER=znc

ARG ZNC_DATADIR=/opt/znc-data

LABEL \
    io.openshift.tags="ZNC" \
    com.redhat.znc.build_date="$LABEL_BUILD_DATE" \
    com.redhat.znc.name="ZNC bouncer" \
    com.redhat.znc.summary="This is image for running \
production version of ZNC in Openshift." \
    com.redhat.znc.url="$LABEL_URL" \
    com.redhat.znc.vcs-type="$LABEL_VCS_TYPE" \
    com.redhat.znc.vcs-url="$LABEL_VCS_URL" \
    com.redhat.znc.vcs-ref="$LABEL_VCS_REF" \
    maintainer="Michal Nováček <mnovacek@redhat.com>"


RUN \
    mkdir -p /entrypoint.d &&\
    yum -y install epel-release &&\
    yum -y install znc openssl &&\
    yum clean all &&\
    rm -rf /var/cache/yum

RUN \
    mkdir -p $ZNC_DATADIR && \
    chown -R $CONTAINER_USER_UID:0 $ZNC_DATADIR && \
    find ${ZNC_DATADIR} \
        -exec chgrp 0 {} \; \
        -exec chmod g+rw {} \; \
        -type d -exec chmod g+x {} \;

COPY znc.conf.d /znc.conf.d
COPY znc.conf /znc.conf-BASE
COPY entrypoint.d/* /entrypoint.d/
COPY entrypoint.sh /entrypoint.sh
RUN \
    chmod 755 /entrypoint.d/*.sh /entrypoint.sh

WORKDIR $ZNC_DATADIR
USER $CONTAINER_USER_UID
# VOLUME $ZNC_DATADIR
ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 12121
