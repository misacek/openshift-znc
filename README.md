
Configuration
=============

ZNC_USE_SSL

znc.conf.d/users
passwords for users in znc are taken from ZNC_PASSWORD_FOR_USER_<user> variable (preferably configmap if run in openshift)

znc.conf.d/listeners


Create openshift application
============================

* have working docker image with working app and port exposed that is NOT
  running as root

* login to oc: oc login -u mnovacek https://open.paas.redhat.com

* cleaning:

    for a in routes services pods dc bc is pvc rc; do oc delete $a --all & done; wait

* create application with dockerfile strategy and insecure registries based on
  git registry and different than master branch

    * Use one of the following:
        * commands

            $ export GITURL=https://gitlab.cee.redhat.com/mnovacek/znc-openshift.git
            $ export GITBRANCH=znc-image-based
            $ oc new-app $GITURL#$GITBRANCH --name=znc \

            $ oc env bc/znc GIT_SSL_NO_VERIFY=true DEBUG=1
            $ oc env dc/znc ZNC_ADMIN_USER=admin

            # create config map entries with passwords for admin, freenode and
            # redhat user and inject them into deployement (todo)
            $ oc env --from=configmap/znc --prefix=ZNC_ADMIN_USER_ dc/znc

            $ oc set probe dc/znc --readiness --get-url=http://:12121
            $ oc set probe dc/znc --liveness -- echo ok

        * add configmap/znc values to deployment config

            $ oc env --from=configmap/znc --prefix=ZNC_ADMIN_PASSWORD_ dc/znc

        * expose service (passthrough route)

            $ oc expose svc/znc \
                --hostname=znc.cloud.upshift.engineering.redhat.com

        * start new build so changed build variables are taken into effect

            $ oc start-build znc --follow

        * template

            $ oc process -f oc.template.yml --parameters
            $ oc process -f oc.template.yml APP_NAME=my-znc-bouncer | oc create -f -

* Once you have it running export what we have remotely to yml::

    oc export all --as-template=my-service-template.yml


Links
=====

How to create container for openshift non-root execution: https://stackoverflow.com/a/42366425

Why is there non-mutable VOLUME and what to do with it: http://l33t.peopleperhour.com/2015/02/18/docker-extending-official-images/
